# FP Has Been Migrated to Github (https://github.com/moneyhub/fp)

# FP

## Functional programming contribution library

Intended to protect us from api and name changes, allow us a single
place to add our contributions, and potentially cherry pick the best
higher order functions from multiple functional libraries.

### Tooling

The tooling for this project is pretty lightweight, and is run via npm.
Currently available tasks:

* test[:watch]
* build
* coverage

There are a couple of githooks in resource/githooks that can be used
to enforce build quality before pushing/committing - please ensure you
install these to ./.git/githooks on your local machine if you wish to
work on this project

the library will transpile from ES6 to ES5 on installation

## API

### attachResult(name, fn, data)

Calls the fn with the data then attaches the result to the data under the supplied name.
This is the same as ```merge({name: fn(data)}, data)``` 

### attachResultP(name, fn, data)

A version of attachResult that can be used in a Promise chain and/or when the function returns a promise.
for example:
 
```
const getAsyncCount = ({n}) => Promise.resolve(n + 1) 
const data = {n: 5}

attachResultP("count", getAsyncCount, data)
// returns Promise[{n: 5, count: 6}]
```

### composeAny([fns]): Promise
Convenience wrapper for avoiding high-ceremony in compose/composeP chains.
Unlike composeP does NOT require that you start with a Promise.  

### composeList([fns])

essentially the same as compose, but requires an array rather than a variable number of arguments


### composeListP([Promises])

see composeList


### either(fn)

returns a function that will return the evaluation of the function, or
the (single) argument that was provided to the function


### inRange(min, max, val)

returns true if ` min <= val < max `, that is, that val is between min and max where min is inclusive and max is exclusive.

### isNilOrEmpty(val)
returns true when val is any of `null`, `undefined`, `""`, `{}`, `[]`

### isObject(val)
returns true when val is a JavaScript object, but not `null` or an array, which both normally have `typeof` being equal to `"object"`

### mapObjKeys(fn, obj)
applies the provided function to each of the keys on the provided object and returns the updated object

### mapObjKeysToCamelCase(obj)
applies the lodash camelCase function to each of the keys on the provided object and returns the updated object

### mapObjKeysToSnakeCase(obj)
applies the lodash snakeCase function to each of the keys on the providd object and returns the updated object

### mapP(options, fn, [items])
Takes a function and a list, applies the function to each of the list's values, and returns
  a list of the same shape.
 
  #####params
 
  - `options Object`
  There is currently only one option
  `{concurrency: concurrencyLimit}`
 
  The concurrency limit applies to Promises returned by the mapper function and it basically
  limits the number of Promises created.
  For example, if concurrency is 3 and the mapper callback has been called enough so that there
  are three returned Promises currently pending, no further callbacks are called until one of
  the pending Promises resolves. So the mapper function will be called three times and it
  will be called again only after at least one of the Promises resolves
 
  - `fn function`
  The function to be called on every element of the input list
 
  - `list Array`
  The list to be iterated over. Can be values or Promises
 
  Returns Array:
  The new list of Promises

### partialMany([fns], a, b, c...)

takes an array of functions, and partially applies the arguments to them

### propIsNil(name, data)
returns true when the named property is Nil

e.g.
`propIsNil("a", {b: 1})` returns true

### propIsNotNil
returns false when the named property is Nil. See `propIsNil`

### pathIsNil
returns true when the property described by the path is Nil

e.g.
`pathIsNil(["a", "b"], {a: {item: "some-data"}})` returns true

### pathIsNotNil
returns false when the property described by the path is Nil. See `pathIsNil`

### propIsNilOrEmpty
returns true when the named property is nil or empty. see `isNilOrEmpty` and `propIsNil`

### pathIsNilOrEmpty
returns true when the path is nil or empty. see `isNilOrEmpty` and `pathIsNil`

### pipeAny
see `composeAny`. Functions processed left to right.

### propWith(notFoundFunction, property, obj)
Returns the value of the property in an object, or runs the property through the supplied function

### rename
Given an object which describes a renaming pattern, and an object to rename, this function will return the 
original object with properties renamed according to the pattern

e.g. 
```
rename({bigness: "size"}, {bigness: 4, species: "Elephant"})

// returns => {size: 4, species: "Elephant"}
```

### safeConcat
Just like concat from ramda, but it does not cause an exception if a non-array
is passed as either of the arguments, instead it acts as if an empty array was passed

### toKeyValuePairs
Returns a new list by pulling every property of the object and transforming
it into a new object with the key and the value of the original property

### throwWhen(predicate, error)(data)
throws the error when the predicate is fulfilled by the data. Otherwise returns the data.

error can be either an Error or an error returning function, in which case it is passed the data 
- this enables throwing an error that describes the cause  

### throwWhenAny
see throwWhen - works inside a promise chain

### when(predicate, operation)

takes a predicate and an operation and produces a function. this
function will return the evaluation of operation if predicate
evaluates to true, or it will return the original value

### whenP

see when. Works inside a promise chain.

### wrapMany([fns], wrapper)

takes an array of functions and wraps them in 'wrapper'


const {compose} = require("ramda")

/* convenience method to partially apply a list of arguments to compose
 *
 * @param {array} list - an array of functions
 * @return {function} composed
 */
module.exports = function composeList(list) {
  return compose.apply(null, list)
}

const {curry, map} = require("ramda")
const throwWhen = require("./throw-when")

module.exports = curry((predicate, error) => map(throwWhen(predicate, error)))

const {pipeP} = require("ramda")
const identityP = require("./identity-p")

// See composeAny for details.
module.exports = (...args) => pipeP(identityP, ...args)

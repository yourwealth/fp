const {curry} = require("ramda")


/**
 * `tryCatch` takes two functions, a `tryer` and a `catcher`. The returned
 * function evaluates the `tryer`; if it does not throw, it simply returns the
 * result. If the `tryer` *does* throw, the returned function evaluates the
 * `catcher` function and returns its result. Note that for effective
 * composition with this function, both the `tryer` and `catcher` functions
 * must return the same type of results.
 *
 * @param {Function} tryer The function that may throw.
 * @param {Function} catcher The function that will be evaluated if `tryer` throws.
 * @return {Function} A new function that will catch exceptions and send then to the catcher.
 * @example
 *
 *      tryCatch(prop('x'), () => true, {x: true}); //=> true
 *      tryCatch(prop('x'), () => false, null);     //=> false
 */
module.exports = curry((tryer, catcher) => {
  try {
    return (...args) => tryer(...args)
  } catch (error) { // eslint-disable-line
    return (...args) => catcher(error, ...args)
  }
})

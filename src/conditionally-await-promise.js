const R = require("ramda")

const conditionallyAwaitPromise = R.curry((willAwait, promise) => {
  return new Promise((resolve) => {
    if (willAwait) resolve(promise)
    resolve(null)
  })
})

/**
 * Bool -> Promise -> Promise
 *
 * Returns the resolved promise if boolean is true, else it resolves to
 * undefined, not waiting for the given promise to resolve.
 */
module.exports = conditionallyAwaitPromise

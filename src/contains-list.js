const {curry, compose, isEmpty, difference, intersection} = require("ramda")

const containsList = curry((targetList, listToLookIn) => {
  return compose(
    isEmpty,
    difference(targetList),
    intersection(targetList)
  )(listToLookIn)
})

/*
 * [a] -> [a] -> Bool
 *
 * Returns true if the first list is a subset of the second list, else false
 */
module.exports = containsList

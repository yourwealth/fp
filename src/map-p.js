const {curry} = require("ramda")
const Promise = require("bluebird")

const mapP = curry((options, fn, list) => {
  return Promise.map(list, fn, options)
})

/*
 * Takes a function and a list, applies the function to each of the list's values, and returns
 * a list of the same shape.
 *
 * mapP :: {k:v} -> (a -> b) ->  [a | Promise a] -> Promise [b]
 *
 * params
 *
 * Object options
 * There is currently only one option
 * {concurrency: concurrencyLimit}
 *
 * The concurrency limit applies to Promises returned by the mapper function and it basically
 * limits the number of Promises created.
 * For example, if concurrency is 3 and the mapper callback has been called enough so that there
 * are three returned Promises currently pending, no further callbacks are called until one of
 * the pending Promises resolves. So the mapper function will be called three times and it
 * will be called again only after at least one of the Promises resolves
 *
 * function fn
 * The function to be called on every element of the input list
 *
 * Array list
 * The list to be iterated over. Can be values or Promises
 *
 * Returns Array
 * The new list of Promises
 *
 * Please see http://bluebirdjs.com/docs/api/promise.map.html
 */
module.exports = mapP

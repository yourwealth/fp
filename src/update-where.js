const {curry, findIndex, whereEq, adjust, mergeRight, __} = require("ramda")

module.exports = curry(function(find, update, data) {
  const index = findIndex(whereEq(find), data)
  return adjust(index, mergeRight(__, update), data)
})

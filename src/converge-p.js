const {composeP, converge, apply} = require("ramda")
const Promise = require("bluebird")

/*
 * (* -> a) -> [(* -> a)] -> [b] -> Promise
 *
 * @param {function} combiner - A function which takes the return
 * values of the list of functions, in the order they were supplied
 * @param {[function]} others - A list of functions, each of which
 * are supplied all arguments applied to convergeP
 *
 * @return function which when called with any number of arguments, returns a Promise
 */
module.exports = (combiner, others) => {
  return composeP(
    apply(combiner),
    converge((...args) => Promise.all(args), others)
  )
}

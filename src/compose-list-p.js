const {composeP} = require("ramda")

/*
 * a convenience function to accept a list of promises
 *
 * @param {array} promises
 * @return {function} composition
 */
module.exports = function(promises) {
  return composeP.apply(null, promises)
}

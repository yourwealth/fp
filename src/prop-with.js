const {curry, prop} = require("ramda")

/*
 * :: (a -> b) -> String -> Object -> b
 * params
 * notFoundFunction  {Function} - The function to call if the named property
 *   is not found on the object
 * property  {String} - The property to search for
 * obj       {Object} - The object to search for the property in
 *
 * Returns the value of the property in an object, or runs the property through
 * the supplied function
 */
const propWith = curry((notFoundFunction, property, obj) => {
  const res = prop(property, obj)
  if (res === undefined) {
    return notFoundFunction(property)
  }
  return res
})

module.exports = propWith

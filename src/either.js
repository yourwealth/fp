const {isNil} = require("ramda")

/* returns the result of fn if it is not undefined, or the original args
 *
 * @param {function} fn
 * @return args | result
 */
module.exports = function either(fn) {
  return function receiver(val) {
    const result = fn(val)
    return isNil(result) ? val : result
  }
}

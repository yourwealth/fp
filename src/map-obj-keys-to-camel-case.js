const mapObjKeys = require("./map-obj-keys")
const {camelCase} = require("lodash")

/**
 * Applies the lodash camelCase function to all of the keys on the target object
 *
 * @param {Object} obj, the object whose keys are going to be mapped to camelCase
 * @return {Object} Returns the object with its keys mapped
 */
module.exports = mapObjKeys(camelCase)

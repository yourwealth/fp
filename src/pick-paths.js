const {curry, reduce, assocPath, path, hasPath} = require("ramda")

/*
 * Picks all the paths from the object specified, and returns a new object with just those paths included
 */
module.exports = curry(function pickPaths(paths, object) {
  return reduce((o, p) =>
    hasPath(p, object)
      ? assocPath(p, path(p, object), o)
      : o
    , {}, paths)
})

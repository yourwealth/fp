const {curry, when, is} = require("ramda")

module.exports = curry((predicate, error) =>
  when(predicate, (arg) => {
    let err = error
    if (is(Function, error)) {
      err = error(arg)
    }

    throw (is(Error, err) ? err : new Error(err))
  }))

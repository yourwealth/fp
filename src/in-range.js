const {complement, curry} = require("ramda")

const inRange = curry(function inRange(min, max, val) {
  return val >= min && val < max
})

const notInRange = complement(inRange)

module.exports = {inRange, notInRange}

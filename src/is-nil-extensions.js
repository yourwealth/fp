const {complement, compose, curryN, isNil, path, prop} = require("ramda")
const isNilOrEmpty = require("./is-nil-or-empty")

const propIsNil = curryN(2, compose(isNil, prop))
const propIsNotNil = complement(propIsNil)

const pathIsNil = curryN(2, compose(isNil, path))
const pathIsNotNil = complement(pathIsNil)

const propIsNilOrEmpty = curryN(2, compose(isNilOrEmpty, prop))

const pathIsNilOrEmpty = curryN(2, compose(isNilOrEmpty, path))

module.exports = {
  propIsNil,
  propIsNotNil,
  pathIsNil,
  pathIsNotNil,
  propIsNilOrEmpty,
  pathIsNilOrEmpty,
}

const {compose, curry, fromPairs, ifElse, map, toPairs} = require("ramda")
const isObject = require("./is-object")

const performMapping = (fn) =>
  compose(
    fromPairs,
    map(([k, v]) => [fn(k), v]),
    toPairs
  )

/**
 * Applies the provided function to all of the keys on the target object
 *
 * @param {function} fn (a -> a), a function that maps from a valid object key name to another
 * @param {Object} obj, the object whose keys are going to be mapped
 * @return {Object} Returns the object with its keys mapped by the function
 */
module.exports = curry((fn, obj) =>
  ifElse(
    isObject,
    performMapping(fn),
    () => {
      throw new Error("mapObjKeys error: Provided input is not an object")
    }
  )(obj)
)

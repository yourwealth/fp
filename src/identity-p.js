const Promise = require("bluebird")

/**
 * Promise identity. Like ramda Identity but for promises.
 * Use at the start of a composeP, or any promise based pipeline, when the
 * first function doesn't need to be a promise.
 * @param  {Object} context The context object in a pipeline.
 * @return {Promise<Object>} Returns the original context, untouched.
 */
module.exports = (context) => Promise.resolve(context)

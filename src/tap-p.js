const Promise = require("bluebird")
const {head, curryN} = require("ramda")

/*
 * ((a, ...) -> *) -> (a, ...) -> Promise<a>
 *
 * Synchronously runs the given function with the supplied value, then returns that value.
 * If an error is thrown, then the error will bubble up.
 *
 * @param {function} fn (a -> *), a function that may produce a result which is not needed in the promise chain
 * @param {*} args
 * @return {Promise<a>} the first argument supplied, wrapped as a resolved Promise.
 */
function tapP(fn, ...args) {
  try {
    const result = fn.apply(null, args)
    return Promise.resolve(result)
      .then(() => head(args))
  } catch (err) {
    return Promise.reject(err)
  }
}

module.exports = curryN(2, tapP)

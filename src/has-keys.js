const {curry, compose, isEmpty, difference, keys, pick} = require("ramda")

const hasKeys = curry((requiredKeys, object) => {
  return compose(
    isEmpty,
    difference(requiredKeys),
    keys,
    pick(requiredKeys)
  )(object)
})

/*
 * Checks if a given object has all the keys specified in the supplied list
 * [a] -> {a: v} -> Bool
 *
 */
module.exports = hasKeys

const {curry, equals, when} = require("ramda")
const composeAny = require("./compose-any")

/*
 * (a -> Bool) -> (a -> b) -> Either(a, b)
 *
 * if 'predicate' evaluates to Promise<true> then returns the result of 'operation'
 * or return the original value passed to the function
 *
 * @param {function} predicate returns true/false as a Promise
 * @param {function} operation
 * @return {function} curried function to accept the value for test
 */
function whenP(predicate, operation) {
  return (value) => composeAny(
    (result) => when(() => equals(true, result), operation)(value),
    predicate
  )(value)
}

module.exports = curry(whenP)

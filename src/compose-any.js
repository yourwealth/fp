const {composeP, init, last} = require("ramda")
const Promise = require("bluebird")

/*
 * Convenience wrapper for avoiding high-ceremony in compose/composeP chains.
 * This function allows you to avoid:
 * -> Promisifying functions that don't need to be.
 * -> Wrapping composeP chains in a try { } catch { } block.
 * -> Forcing a Promise placeholder (_P) as the first arg of a composeP chain.
 * This function works on promise chains, non-promise chains, and mixed chains.
 * @param  {Rest<Function>} ...args Function arguments (for the compose chain)
 * @return {Promise<Any>}           Returns a promise containing the chain result.
 */
module.exports = function composeAny(...fns) {
  function lastFn(...args) {
    const _last = last(fns)
    return new Promise((resolve, reject) => {
      try {
        resolve(_last.apply(null, args))
      } catch (err) {
        reject(err)
      }
    })
  }
  return composeP(
    ...init(fns),
    lastFn
  )
}

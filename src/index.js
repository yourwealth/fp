const attachResultP = require("./attach-result-p")
const attachResult = require("./attach-result")
const conditionallyAwaitPromise = require("./conditionally-await-promise")
const composeAny = require("./compose-any")
const composeList = require("./compose-list")
const composeListP = require("./compose-list-p")
const containsList = require("./contains-list")
const convergeP = require("./converge-p")
const either = require("./either")
const hasKeys = require("./has-keys")
const identityP = require("./identity-p")
const {inRange, notInRange} = require("./in-range")
const isNilOrEmpty = require("./is-nil-or-empty")
const isObject = require("./is-object")
const mapObjKeys = require("./map-obj-keys")
const mapObjKeysToCamelCase = require("./map-obj-keys-to-camel-case")
const mapObjKeysToSnakeCase = require("./map-obj-keys-to-snake-case")
const mapP = require("./map-p")
const partialMany = require("./partial-many")
const pickPaths = require("./pick-paths")
const pipeAny = require("./pipe-any")
const propWith = require("./prop-with")
const removeUndefined = require("./remove-undefined")
const rename = require("./rename")
const round = require("./round")
const safeConcat = require("./safe-concat")
const tapP = require("./tap-p")
const throwWhen = require("./throw-when")
const throwWhenAny = require("./throw-when-any")
const toKeyValuePairs = require("./to-key-value-pairs")
const tryCatch = require("./try-catch")
const updateWhere = require("./update-where")
const whenP = require("./when-p")
const {greaterThan, greaterThanOrEqual, lessThan, lessThanOrEqual} = require("./comparisons")
const {propIsNil, propIsNotNil, pathIsNil, pathIsNotNil, propIsNilOrEmpty, pathIsNilOrEmpty} = require("./is-nil-extensions")

const library = {
  attachResult,
  attachResultP,
  attachResultAny: attachResultP,
  composeAny,
  composeList,
  composeListP,
  conditionallyAwaitPromise,
  containsList,
  convergeAny: convergeP,
  convergeP,
  either,
  greaterThan,
  greaterThanOrEqual,
  hasKeys,
  identityP,
  inRange,
  notInRange,
  isNilOrEmpty,
  isObject,
  lessThan,
  lessThanOrEqual,
  mapObjKeys,
  mapObjKeysToCamelCase,
  mapObjKeysToSnakeCase,
  mapP,
  partialMany,
  pathIsNil,
  pathIsNotNil,
  pathIsNilOrEmpty,
  pickPaths,
  pipeAny,
  propIsNil,
  propIsNotNil,
  propIsNilOrEmpty,
  propWith,
  removeUndefined,
  rename,
  round,
  safeConcat,
  tapP,
  throwWhen,
  throwWhenAny,
  toKeyValuePairs,
  tryCatch,
  updateWhere,
  whenP,
}

module.exports = library

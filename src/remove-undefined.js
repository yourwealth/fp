const {pickBy, complement, equals} = require("ramda")

module.exports = pickBy(complement(equals(undefined)))

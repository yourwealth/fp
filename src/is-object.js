const R = require("ramda")

const isInstanceOfObject = R.is(Object)
const isntInstanceOfArray = R.complement(R.is(Array))
const isntInstanceOfFunction = R.complement(R.is(Function))
const isObject = R.allPass([isInstanceOfObject, isntInstanceOfArray, isntInstanceOfFunction])

/**
 * Checks if the input is a JavaScript object, and not an array, function, or null
 *
 * @param {Object} obj, the object to check
 * @return {boolean} Returns whether or not the input is an object
 */
module.exports = isObject


const {either, isNil, isEmpty} = require("ramda")

module.exports = either(isNil, isEmpty)

const {partial, map} = require("ramda")

/*
 * patrially applies all supplied functions with the arguments
 *
 * @param {array} fns
 * @param {array} args
 * @param {array}
 */
function partialMany(fns, ...args) {
  return map((fn) => {
    return partial(fn, args)
  })(fns)
}

module.exports = partialMany

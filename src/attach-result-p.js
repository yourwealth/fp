const {curry, mergeRight, objOf} = require("ramda")
const composeAny = require("./compose-any")

/*
 * Calls the fn with the data then attaches the result to the data under the supplied attachName.
 */
module.exports = curry(function attachResultP(attachName, fn, data) {
  return composeAny(
    mergeRight(data),
    objOf(attachName),
    fn
  )(data)
})

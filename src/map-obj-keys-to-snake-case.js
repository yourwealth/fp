const mapObjKeys = require("./map-obj-keys")
const {snakeCase} = require("lodash")

/**
 * Applies the lodash snakeCase function to all of the keys on the target object
 *
 * @param {Object} obj, the object whose keys are going to be mapped to snakeCase
 * @return {Object} Returns the object with its keys mapped
 */
module.exports = mapObjKeys(snakeCase)

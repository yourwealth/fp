const {curry, concat} = require("ramda")
const {isArrayLike} = require("ramda-adjunct")

/*
 * Just like concat from ramda, but it does not cause an exception if a non-array
 * is passed as either of the arguments, instead it acts as if an empty array was passed
 */
module.exports = curry((arg1, arg2) =>
  concat(
    isArrayLike(arg1) ? arg1 : [],
    isArrayLike(arg2) ? arg2 : [],
  )
)

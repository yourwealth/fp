const {compose, curry, mergeRight, objOf} = require("ramda")

/*
 * Calls the fn with the data then attaches the result to the data under the supplied attachName.
 */
module.exports = curry(function attachResult(attachName, fn, data) {
  return compose(
    mergeRight(data),
    objOf(attachName),
    fn
  )(data)
})

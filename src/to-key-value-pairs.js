const {compose, map, toPairs} = require("ramda")

/*
 * :: a -> [b]
 * params
 * obj     Object   The object to consider
 *
 * Returns a new list by pulling every property of the object and transforming
 * it into a new object with the key and the value of the original property
 *
 *  toKeyValuePairs({ a: 1, b: 2})
 * // => [ { key: 'a', value: 1 }, { key: 'b', value: 2 } ]
 */
const toKeyValuePairs = compose(map((pair) => {
  return {
    key: pair[0],
    value: pair[1],
  }
}), toPairs)

module.exports = toKeyValuePairs


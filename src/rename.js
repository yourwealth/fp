const {compose, keys, mergeRight, omit, toPairs, curry, reduce, __, reject,
  fromPairs, apply, equals, converge} = require("ramda")

const addRenamedProperty = curry(function(originalObject, memo, pair) {
  memo[pair[1]] = originalObject[pair[0]]
  return memo
})

function renameAndMerge(renamingPattern, originalObject) {
  return compose(
    mergeRight(__, originalObject),
    reduce(addRenamedProperty(originalObject), {}),
    toPairs
  )(renamingPattern)
}

function removeSelfMappingKeys(pattern) {
  return compose(
    fromPairs,
    reject(apply(equals)),
    toPairs
  )(pattern)
}

const rename = curry(function(renamingPattern, originalObject) {
  return converge(
    omit,
    [keys, renameAndMerge]
  )(removeSelfMappingKeys(renamingPattern), originalObject)
})

// {k:v} -> {k:v} -> {k:v}
// Renames properties on an object
//
// Given an object which describes a renaming pattern, and an object
// to rename, this function will return the original object with
// properties renamed according to the pattern
//
// e.g. rename {bigness: "size"}, {bigness: 4, species: "Elephant"}
// returns => {size: 4, species: "Elephant"}
module.exports = rename

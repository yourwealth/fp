const {gt, gte, lt, lte} = require("ramda")

module.exports = {
  greaterThan: lt,
  greaterThanOrEqual: lte,
  lessThan: gt,
  lessThanOrEqual: gte,
}

const {expect} = require("chai")
const {hasKeys} = require("../src/")

describe("hasKeys", () => {
  it("returns false if none of the keys exist", () =>  {
    expect(hasKeys(["a", "b"], {"c": 1})).to.equal(false)
  })

  it("returns false if not all of the keys exist", () =>  {
    expect(hasKeys(["a", "b"], {"a": 1})).to.equal(false)
  })

  it("returns true if all of the keys exist", () =>  {
    expect(hasKeys(["a", "b"], {"a": 1, "b": 1})).to.equal(true)
  })

  it("returns true if all of the keys exist, plus extras", () =>  {
    expect(hasKeys(["a", "b"], {"a": 1, "b": 1, "c": 1})).to.equal(true)
  })
})

const {expect} = require("chai")
const {pickPaths} = require("../src")

describe("pickPaths", function() {
  it("should extract the paths from the object into a new object", function() {

    const paths = [
      ["top", "one"],
      ["top", "two"],
      ["top", "undef"],
      ["top", "nool"],
      ["missing"],
      ["missing", "two"],
      ["top2", "missing2"],
      ["top3"],
      ["topUndef"],
      ["topNull"],
      ["topSimple"],
      ["topFn"],
    ]
    const object = {
      top: {
        one: "1", two: 2, three: "3", undef: undefined, nool: null,
      },
      top2: {
        four: "4",
      },
      top3: {
        five: 5,
        many: {many: "many"},
      },
      topUndef: undefined,
      topNull: null,
      topSimple: "simple",
      topFn: expect,
    }

    const res = pickPaths(paths, object)

    expect(res).to.eql({
      top: {
        one: "1", two: 2, undef: undefined, nool: null,
      },
      top3: {
        five: 5,
        many: {many: "many"},
      },
      topUndef: undefined,
      topNull: null,
      topSimple: "simple",
      topFn: expect,
    })
  })

  it("should extract paths from an empty object into a new empty object", function() {

    const paths = [
      ["top", "one"],
      ["top", "two"],
    ]
    const object = {}

    const res = pickPaths(paths, object)

    expect(res).to.eql({})
  })

  it("should extract no paths from an object into a new empty object", function() {

    const paths = []
    const object = {top: 1}

    const res = pickPaths(paths, object)

    expect(res).to.eql({})
  })
})

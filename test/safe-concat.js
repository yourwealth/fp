const {expect} = require("chai")
const {safeConcat} = require("../src/")

describe("safeConcat", () => {
  it("is a function", () => {
    expect(safeConcat).to.be.a("function")
  })

  it("both arguments are arrays, concat them", () => {
    expect(safeConcat([1, 2], [3, 4])).to.eql([1, 2, 3, 4])
  })

  it("first argument is [undefined, null, object], return the other one", () => {
    expect(safeConcat(undefined, [3, 4])).to.eql([3, 4])
    expect(safeConcat(null, [3, 4])).to.eql([3, 4])
    expect(safeConcat({}, [3, 4])).to.eql([3, 4])
  })

  it("second argument is [undefined, null, object], return the other one", () => {
    expect(safeConcat([3, 4], undefined)).to.eql([3, 4])
    expect(safeConcat([3, 4], null)).to.eql([3, 4])
    expect(safeConcat([3, 4], {})).to.eql([3, 4])
  })

  it("both arguments are [undefined, null, object], return empty array", () => {
    expect(safeConcat(undefined, undefined)).to.eql([])
    expect(safeConcat(null, null)).to.eql([])
    expect(safeConcat({}, {})).to.eql([])
  })

  it("should return a curried function", () => {
    expect(safeConcat([1, 2])).to.be.a("function")
  })
})

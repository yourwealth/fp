const {expect} = require("chai")
const {pipeAny} = require("../src/")

describe("pipeAny", () =>  {

  const voidFunc = () => {}
  const callCounter = (count) => count + 1

  it("is a function", () => expect(pipeAny).to.be.a("function"))

  it("returns a Promise", () => expect(pipeAny(voidFunc)({}).then).to.be.a("function"))

  it("runs function arguments", () => {
    return pipeAny(
      callCounter,
      callCounter,
      callCounter)(0)
      .then(result => {
        expect(result).to.equal(3)
      })
  })
})

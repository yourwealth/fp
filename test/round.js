const {expect} = require("chai")
const {round} = require("../src")

describe("round", function() {
  it("should round a number to a certain number of decimals", function() {
    const res = round(2, 1.3456)

    expect(res).to.eql(1.35)
  })
})

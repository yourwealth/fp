const {expect} = require("chai")
const {containsList} = require("../src")

describe("containsList", () => {
  it("returns false if the target list does not exist in the other list", () => {
    expect(containsList([1, 2], [3, 4])).to.equal(false)
  })

  it("returns false if the target list does not fully exist in the other list", () => {
    expect(containsList([1, 2], [2, 4])).to.equal(false)
  })

  it("returns true if the target list is the same as the other list", () => {
    expect(containsList([1, 2], [1, 2])).to.equal(true)
  })

  it("returns true if the target list is found in the other list", () => {
    expect(containsList([1, 2], [1, 2, 3, 4])).to.equal(true)
  })

  it("order doesn't matter", () => {
    expect(containsList([1, 1], [1, 2, 3, 4])).to.equal(true)
  })
})

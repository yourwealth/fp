/* eslint-disable max-nested-callbacks */

const {expect} = require("chai")
const {whenP} = require("../src/")
const Promise = require("bluebird")

describe("whenP", () => {

  it("is a function", () => {
    expect(whenP).to.be.a("function")
  })

  it("produces a function", () => {
    const whenPFn = whenP(() => {}, () => {})
    expect(whenPFn).to.be.a("function")
  })

  describe("whenP receives predicate and operation", () => {
    let whenIsOne
    beforeEach(() => {
      whenIsOne = whenP(v => Promise.resolve(v === 1), () => "is one")
    })

    it("if predicate evaluates to Promise(true), it will return the result of the operation", () => {
      return whenIsOne(1).then((result) => {
        expect(result).to.equal("is one")
      })
    })

    it("if predicate evaluates to Promise(false), it will return the original value", () => {
      return whenIsOne(2).then((result) => {
        expect(result).to.equal(2)
      })
    })
  })

  describe("the operation receives the value given to when", () => {
    let whenIsOneMakeSeventeen
    beforeEach(() => {
      whenIsOneMakeSeventeen = whenP(v => Promise.resolve(v === 1), v => v * 17)
    })

    it("if predicate evaluates to Promise(true), it will return the result of the operation", () => {
      return whenIsOneMakeSeventeen(1).then((result) => {
        expect(result).to.equal(17)
      })
    })
  })

  it("this function is curried, and expects two arguments", () => {
    const whenIsOneA = whenP(v => v === 1)
    expect(whenIsOneA).to.be.a("function")
    const whenIsOneB = whenIsOneA(() => "is one")
    expect(whenIsOneB).to.be.a("function")
    const result = whenIsOneB(1)
    return result.then((r) => {
      expect(r).to.equal("is one")
    })
  })

})

const {expect} = require("chai")
const {partialMany} = require("../src/")

describe("partialMany", () =>  {
  it("is a function", () => {
    expect(partialMany).to.be.a("function")
  })

  it("will partially apply the list of functions provided", () => {
    const [foo, bar, baz] = partialMany([
      (a) => expect(a).to.equal(1),
      (a, b) => expect(b).to.equal(2),
      (a, b, c) => expect(c).to.equal(3),
    ], 1, 2, 3)
    foo()
    bar()
    baz()
  })
})


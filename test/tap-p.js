const tapP = require("../src/tap-p")
const chai = require("chai")
const sinon = require("sinon")
const {composeP} = require("ramda")
const Promise = require("bluebird")
chai.use(require("sinon-chai"))
const {expect} = chai

describe("tapP", () => {

  const voidFunc = () => {}
  const funcP = () => Promise.resolve({})
  const inc = (count) => count + 1
  let method, result

  it("is a function", () => expect(tapP).to.be.a("function"))

  it("returns a Promise for fn that is not promise returning", () => expect(tapP(voidFunc)({})).to.be.instanceOf(Promise))

  it("returns a Promise for fn that is promise returning", () => expect(tapP(funcP)({})).to.be.instanceOf(Promise))

  it("the original argument should be returned, not the result of tapP", () => {
    return tapP(inc)(1).then((r) => expect(r).to.equal(1))
  })

  it("should cope with being passed null", () => {
    return tapP(inc)(null).then((r) => expect(r).to.equal(null))
  })

  it("should be unchanged when called with no args", () => {
    const curriedTapP = tapP(inc)()
    return curriedTapP(1).then((r) => expect(r).to.equal(1))
  })

  describe("when pass multiple args", () => {

    beforeEach(() => {
      method = sinon.stub().returns(5)
      return tapP(method)(1, 2, 3, 4, 5)
    })

    it("should call method with all args", () => {
      expect(method).to.have.been.calledWith(1, 2, 3, 4, 5)
    })
  })

  describe("when inside a composeP", () => {

    let method2

    beforeEach(() => {
      method = sinon.stub().resolves(5)
      method2 = sinon.stub().returns(2)

      result = composeP(
        inc,
        method2,
        tapP(method)
      )(1, 2)
      return result
    })

    it("method in tapP should not effect result", () => {
      return result.then((r) => {
        expect(r).to.equal(3)
      })
    })

    it("should call the method after it with the first arg of compose", () => {
      expect(method2).to.have.been.calledWithExactly(1)
    })

    it("should complete synchronously", () => {
      expect(method).to.have.been.calledBefore(method2)
    })
  })

  describe("when method rejects with an error", () => {
    let method2
    const error = new Error({message: "error"})

    beforeEach(() => {
      method = sinon.stub().rejects(error)
      method2 = sinon.stub().returns(2)

      result = composeP(
        method2,
        tapP(method)
      )(1, 2)
    })

    it("should return a rejection", () => {
      return result.catch((err) => expect(err).to.equal(error))
    })

    it("should not call the subsequent methods", () => {
      return result.catch(() => expect(method2).to.not.have.been.called)
    })
  })

  describe("when method throws an error", () => {
    let method2
    const error = new Error({message: "error"})

    beforeEach(() => {
      method = () => {
        throw error
      }
      method2 = sinon.stub().returns(2)

      result = composeP(
        method2,
        tapP(method)
      )(1, 2)
    })

    it("should return a rejection", () => {
      return result.catch((err) => expect(err).to.equal(error))
    })

    it("should not call the subsequent methods", () => {
      return result.catch(() => expect(method2).to.not.have.been.called)
    })
  })

  describe("when it is a long wait", () => {
    let method2, sideEffect

    beforeEach(() => {
      method2 = () => {
        sideEffect += "-second-method"
        return sideEffect
      }
      sideEffect = undefined

      const longPromise = () => new Promise(resolve =>
        setTimeout(() => {
          sideEffect = "tap-first-effected"
          resolve({})
        }, 1000)
      )

      result = tapP(longPromise, 1).then(method2)
      return result
    })

    it("should return the result", () => {
      return result.then((r) => {
        expect(r).to.equal("tap-first-effected-second-method")
      })
    })

    it("should wait for promise to resolve before continuing with next method", () => {
      expect(sideEffect).to.equal("tap-first-effected-second-method")
    })
  })

})

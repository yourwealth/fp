/* eslint-disable max-nested-callbacks */

const {expect} = require("chai")
const {throwWhen} = require("../src/")

function makeError({message}) {
  return new Error(message)
}

describe("throwWhen", () => {

  it("is a function",
    () => expect(throwWhen).to.be.a("function"))

  it("returns a function",
    () => expect(throwWhen(() => true, "")).to.be.a("function"))

  it("throws an error when passed a predicate which evaluates to true",
    () => expect(() => throwWhen(() => true, new Error())({})).to.throw(Error)
  )

  it("returns the original value when passed a predicate which evaluates to false",
    () => expect(throwWhen(() => false, "error")({a: 1})).to.eql({a: 1})
  )

  it("supplies the arg to the error creating function when it is a function", () => {
    expect(() => throwWhen(() => true, makeError)({message: "error text!"})).to.throw(Error, "error text!")
  })
})

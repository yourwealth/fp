const {expect} = require("chai")
const {conditionallyAwaitPromise} = require("../src")

describe("conditionallyAwaitPromise", () => {
  it("resolves to given promise if set to true", async () => {
    const result = await conditionallyAwaitPromise(true, Promise.resolve("resolve"))
    expect(result).to.equal("resolve")
  })

  it("resolves to null if set to false", async () => {
    const result = await conditionallyAwaitPromise(false, Promise.resolve("resolve"))
    expect(result).to.be.null
  })

  it("resolves to given promise if set to true, curried", async () => {
    const result = await conditionallyAwaitPromise(true)(Promise.resolve("resolve"))
    expect(result).to.equal("resolve")
  })

  it("resolves to null if set to false, curried", async () => {
    const result = await conditionallyAwaitPromise(false)(Promise.resolve("resolve"))
    expect(result).to.be.null
  })
})

const chai = require("chai")
const composeAny = require("../src/compose-any.js")
const sinon = require("sinon")
chai.use(require("sinon-chai"))
const {expect} = chai
const Promise = require("bluebird")

describe("composeAny", () =>  {
  const voidFunc = () => {}
  const increment = (count) => count + 1
  const add = (a, b) => a + b
  const addP = (a, b) => Promise.resolve(a + b)
  const addPError = new Error("add P error")
  const addPErrors = () => Promise.reject(addPError)

  it("is a function", () => expect(composeAny).to.be.a("function"))

  it("returns a Promise", () => expect(composeAny(voidFunc)({})).to.be.instanceOf(Promise))

  it("runs function arguments", () => {
    return composeAny(
      increment,
      increment,
      increment
    )(0)
      .then(result => {
        expect(result).to.equal(3)
      })
  })

  it("handles multiple arguments", () => {
    return composeAny(
      increment,
      increment,
      add
    )(2, 2)
      .then(result => {
        expect(result).to.equal(6)
      })
  })

  it("handles promises", () => {
    return composeAny(
      increment,
      increment,
      addP
    )(2, 2)
      .then(result => {
        expect(result).to.equal(6)
      })
  })

  it("handles promises when they error", () => {
    return composeAny(
      increment,
      increment,
      addPErrors
    )(2, 2)
      .catch(err => {
        expect(err).to.equal(addPError)
      })
  })

  describe("when fn throws error", () => {
    let result
    const error = new Error({message: "can't add"})

    beforeEach(() => {
      result = composeAny(
        increment,
        increment,
        () => {
          throw error
        }
      )(2, 2)
    })

    it("handles functions which throw errors", () => {
      return result.catch(err => {
        expect(err).to.equal(error)
      })
    })
  })

  describe("when you compose a composeAny and there is an inner error", () => {
    let method, result
    const error = new Error("inner error")

    beforeEach(() => {
      method = sinon.stub()

      result = composeAny(
        method,
        composeAny(
          () => {
            throw new Error("another error")
          },
          method,
          () => {
            throw error
          }
        )
      )(2, 2)
    })

    it("should throw the error up the chain", () => {
      return result.catch(err => {
        expect(err).to.equal(error)
      })
    })

    it("should not call any methods that occur after the error", () => {
      return result.catch(() => {
        expect(method).to.not.have.been.called
      })
    })
  })
})

/* eslint-disable max-nested-callbacks */

const Promise = require("bluebird")
const chai = require("chai")
const {expect} = chai
const attachResultP = require("../src/attach-result-p")
const sinon = require("sinon")
chai.use(require("sinon-chai"))

describe("attachResultP", () => {

  it("should be a function", () => {
    expect(attachResultP).to.be.instanceOf(Function)
  })

  it("should return a function, curry_3", () => {
    expect(attachResultP("x")).to.be.instanceOf(Function)
    expect(attachResultP("x")("y")).to.be.instanceOf(Function)
    expect(attachResultP("x", "y")).to.be.instanceOf(Function)
  })

  describe("when using a Promise returning function that resolves", () => {
    let result
    let fn

    beforeEach(() => {
      fn = sinon.stub().resolves(1)
      result = attachResultP("x", fn)({data: "data"})
      return result
    })

    it("should return a Promise", () => {
      expect(result).to.be.instanceOf(Promise)
    })

    it("should merge a promise returning function result by the supplied name", () => {
      return result.then((r) => expect(r).to.eql({
        x: 1,
        data: "data",
      }))
    })

    it("should call the promise returning function with the data", () => {
      expect(fn).to.have.been.calledWith({data: "data"})
    })
  })

  describe("when using a Promise returning function that rejects", () => {
    let result
    let fn
    beforeEach(() => {
      fn = sinon.stub().rejects("errror")
      result = attachResultP("x", fn)({data: "data"})
    })

    it("should merge a promise returning function result by the supplied name", () => {
      return result.catch((r) => expect(r).to.eql("errror"))
    })
  })

  describe("when using a non-promise returning function", () => {
    let result
    let fn
    beforeEach(() => {
      fn = sinon.stub().returns(1)
      result = attachResultP("x", fn, {data: "data"})
      return result
    })

    it("should return a Promise", () => {
      expect(result).to.be.instanceOf(Promise)
    })

    it("should merge the result by the supplied name", () => {
      return result.then((r) => expect(r).to.eql({
        x: 1,
        data: "data",
      }))
    })

    it("should call the function with the data", () => {
      expect(fn).to.have.been.calledWith({data: "data"})
    })
  })
})

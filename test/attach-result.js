const chai = require("chai")
const {expect} = chai
const attachResult = require("../src/attach-result")

chai.use(require("sinon-chai"))

describe("attachResult", () => {

  it("should be a function", () => {
    expect(attachResult).to.be.instanceOf(Function)
  })

  describe("when using a Promise returning function that resolves", () => {
    let result
    let fn

    beforeEach(() => {
      fn = (a) => a
      result = attachResult("x", fn)({data: "data"})
      return result
    })

    it("should merge result of fn to response under a prop named via parameter", () => {
      return expect(result).to.eql({
        x: {data: "data"},
        data: "data",
      })
    })
  })
})

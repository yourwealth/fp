/* eslint-disable max-nested-callbacks */

const {inc, range, last, sortBy, compose, identity} = require("ramda")
const {expect} = require("chai")
const mapP = require("../src/map-p")

function incP(num) {
  return new Promise(resolve => resolve(num + 1))
}

describe("mapP", () => {
  it("is a function", () => expect(mapP).to.be.a("function"))

  it("returns a Promise when given an empty list", () => {
    expect(mapP({concurrency: 1}, inc, []).then).to.be.a("function")
  })

  describe("with a function returning a value", () => {
    it("when a list of values is applied, returns a promise with a list of values", () => {
      return mapP({concurrency: 1}, inc, [1])
        .then(result => {
          expect(result).to.eql([2])
        })
    })

    it("when a list of promises are applied, returns a promise with a list of values", () => {
      return mapP({concurrency: 1}, inc, [Promise.resolve(10)])
        .then(result => {
          expect(result).to.eql([11])
        })
    })
  })

  describe("with a function returning a promise", () => {
    it("when a list of values is applied, returns a promise with a list of values", () => {
      return mapP({concurrency: 1}, incP, [1])
        .then(result => {
          expect(result).to.eql([2])
        })
    })

    it("when a list of promises are applied, returns a promise with a list of values", () => {
      return mapP({concurrency: 1}, incP, [Promise.resolve(10)])
        .then(result => {
          expect(result).to.eql([11])
        })
    })

    it("adheres to the concurrency limit", () => {
      const concurrentCounts = []
      let currentRunning = 0

      function recordStarted() {
        currentRunning += 1
        concurrentCounts.push(currentRunning)
      }

      function recordFinished() {
        currentRunning -= 1
        concurrentCounts.push(currentRunning)
      }

      function delayedInc(item) {
        return new Promise((resolve) => {
          recordStarted()
          setTimeout(() => {
            recordFinished()
            resolve(item + 1)
          }, 10)
        })
      }

      return mapP({concurrency: 2}, delayedInc, range(0, 10))
        .then(result => {
          expect(result).to.eql([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

          const maxNumberOfConcurrentFunctions = compose(last, sortBy(identity))(concurrentCounts)
          expect(maxNumberOfConcurrentFunctions).to.eql(2)
        })
    })
  })
})

const {expect} = require("chai")
const {rename} = require("../src/")

describe("rename", () =>  {
  it("renames a single item", () => {
    const result = rename({foo: "bar"}, {foo: 1})
    expect(result).to.eql({bar: 1})
  })

  it("renames multiple items", () => {
    const result = rename({foo: "foo1", bar: "bar1"}, {foo: 1, bar: 2})
    expect(result).to.eql({foo1: 1, bar1: 2})
  })

  it("renames multiple items, ignores self-mapping keys", () => {
    const result = rename({foo: "foo1", bar: "bar1", baz: "baz"}, {foo: 1, bar: 2, baz: 3})
    expect(result).to.eql({foo1: 1, bar1: 2, baz: 3})
  })
})

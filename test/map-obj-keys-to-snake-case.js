const chai = require("chai")
const {expect} = chai
const mapObjKeysToSnakeCase = require("../src/map-obj-keys-to-snake-case")

describe("mapObjKeysToSnakeCase", () => {

  it("should be a function", () => {
    expect(mapObjKeysToSnakeCase).to.be.instanceOf(Function)
  })

  it("should return an object with converted keys", () => {
    const input = {
      thisIsCamelCase: 5,
      "this-is-kebab-case": 2,
      "this_is_snake_case": 6,
      "this5@_has4some$symbolsAnd_numbers": 10,
    }
    const expected = {
      "this_is_camel_case": 5,
      "this_is_kebab_case": 2,
      "this_is_snake_case": 6,
      "this_5_has_4_some_symbols_and_numbers": 10,
    }
    expect(mapObjKeysToSnakeCase(input)).to.deep.equal(expected)
  })

  it("should throw when array", () => {
    const input = [{
      thisHASaMixOfUpPeraNdLoWeR: 5,
      thisisalllower: 10,
      THISISALLUPPER: 1,
      this5HAs9$aCoMBo: 11,
    }]

    expect(() => mapObjKeysToSnakeCase(input)).to.throw("mapObjKeys error: Provided input is not an object")
  })

  it("should throw when string", () => {
    const input = "test_this"
    expect(() => mapObjKeysToSnakeCase(input)).to.throw("mapObjKeys error: Provided input is not an object")
  })

  it("should throw when number", () => {
    const input = 5
    expect(() => mapObjKeysToSnakeCase(input)).to.throw("mapObjKeys error: Provided input is not an object")
  })

  it("should throw when null", () => {
    const input = null
    expect(() => mapObjKeysToSnakeCase(input)).to.throw("mapObjKeys error: Provided input is not an object")
  })

  it("should throw when undefined", () => {
    const input = undefined
    expect(() => mapObjKeysToSnakeCase(input)).to.throw("mapObjKeys error: Provided input is not an object")
  })
})

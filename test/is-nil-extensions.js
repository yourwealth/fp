const {expect} = require("chai")
const {propIsNil, propIsNotNil, pathIsNil, pathIsNotNil, propIsNilOrEmpty, pathIsNilOrEmpty} = require("../src/is-nil-extensions")

describe("propIsNil", () => {

  it("should be a function", () => {
    expect(propIsNil).to.be.instanceOf(Function)
  })

  it("should return a function", () => {
    expect(propIsNil("name")).to.be.instanceOf(Function)
  })

  it("should evaluate if a property is nil", () => {
    expect(propIsNil("a")({a: 1})).to.equal(false)
    expect(propIsNil("b")({a: 1})).to.equal(true)
    expect(propIsNil("b")({b: null})).to.equal(true)
    expect(propIsNil("b")({b: undefined})).to.equal(true)
  })
})

describe("propIsNotNil", () => {

  it("should be a function", () => {
    expect(propIsNotNil).to.be.instanceOf(Function)
  })

  it("should return a function", () => {
    expect(propIsNotNil("name")).to.be.instanceOf(Function)
  })

  it("should evaluate if a property is nil", () => {
    expect(propIsNotNil("a")({a: 1})).to.equal(true)
    expect(propIsNotNil("b")({a: 1})).to.equal(false)
    expect(propIsNotNil("b")({b: null})).to.equal(false)
    expect(propIsNotNil("b")({b: undefined})).to.equal(false)
  })
})

describe("pathIsNil", () => {

  it("should be a function", () => {
    expect(pathIsNil).to.be.instanceOf(Function)
  })

  it("should return a function", () => {
    expect(pathIsNil(["name"])).to.be.instanceOf(Function)
  })

  it("should evaluate if a path is nil", () => {
    expect(pathIsNil(["a"])({a: 1})).to.equal(false)
    expect(pathIsNil(["a", "b"])({a: {b: 1}})).to.equal(false)
    expect(pathIsNil(["a", "b"])({a: 1})).to.equal(true)
    expect(pathIsNil(["b"])({b: null})).to.equal(true)
    expect(pathIsNil(["b"])({b: undefined})).to.equal(true)
  })
})

describe("pathIsNotNil", () => {

  it("should be a function", () => {
    expect(pathIsNotNil).to.be.instanceOf(Function)
  })

  it("should return a function", () => {
    expect(pathIsNotNil("name")).to.be.instanceOf(Function)
  })

  it("should evaluate if a path is nil", () => {
    expect(pathIsNotNil(["a"])({a: 1})).to.equal(true)
    expect(pathIsNotNil(["a", "b"])({a: {b: 1}})).to.equal(true)
    expect(pathIsNotNil(["a", "b"])({a: 1})).to.equal(false)
    expect(pathIsNotNil(["b"])({b: null})).to.equal(false)
    expect(pathIsNotNil(["b"])({b: undefined})).to.equal(false)
  })
})


describe("propIsNilOrEmpty", () => {

  it("should be a function", () => {
    expect(propIsNilOrEmpty).to.be.instanceOf(Function)
  })

  it("should return a function", () => {
    expect(propIsNilOrEmpty("name")).to.be.instanceOf(Function)
  })

  it("should evaluate if a property is nil or empty", () => {
    expect(propIsNilOrEmpty("a")({a: 1})).to.equal(false)
    expect(propIsNilOrEmpty("b")({a: 1})).to.equal(true)
    expect(propIsNilOrEmpty("b")({b: null})).to.equal(true)
    expect(propIsNilOrEmpty("b")({b: undefined})).to.equal(true)

    expect(propIsNilOrEmpty("b")({b: []})).to.equal(true)
    expect(propIsNilOrEmpty("b")({b: [1]})).to.equal(false)
  })
})

describe("pathIsNilOrEmpty", () => {

  it("should be a function", () => {
    expect(pathIsNilOrEmpty).to.be.instanceOf(Function)
  })

  it("should return a function", () => {
    expect(pathIsNilOrEmpty(["name"])).to.be.instanceOf(Function)
  })

  it("should evaluate if a path is nil", () => {
    expect(pathIsNilOrEmpty(["a"])({a: 1})).to.equal(false)
    expect(pathIsNilOrEmpty(["a", "b"])({a: {b: 1}})).to.equal(false)
    expect(pathIsNilOrEmpty(["a", "b"])({a: 1})).to.equal(true)
    expect(pathIsNilOrEmpty(["b"])({b: null})).to.equal(true)
    expect(pathIsNilOrEmpty(["b"])({b: undefined})).to.equal(true)

    expect(pathIsNilOrEmpty(["b"])({b: []})).to.equal(true)
    expect(pathIsNilOrEmpty(["b"])({b: [1]})).to.equal(false)
  })
})

const {expect} = require("chai")
const {identity} = require("ramda")
const {propWith} = require("../src/")

describe("propWith", () =>  {
  it("is a function", () => {
    expect(propWith).to.be.a("function")
  })

  it("returns the value of the property if found", () => {
    expect(propWith(identity, "foo", {foo: "bar"})).to.equal("bar")
  })

  it("returns the result of running the function of the property is not found", () => {
    function getError(property) {
      return "could not find " + property
    }
    expect(propWith(getError, "baz", {foo: "bar"})).to.equal("could not find baz")
  })
})

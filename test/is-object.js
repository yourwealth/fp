const chai = require("chai")
const {expect} = chai
const isObject = require("../src/is-object")

describe("isObject", () => {

  it("should be a function", () => {
    expect(isObject).to.be.instanceOf(Function)
  })

  it("should return true if object", () => {
    const input = {
      thisIsCamelCase: 5,
      "this-is-kebab-case": 2,
      "this_is_snake_case": 6,
      "this5@_has4some$symbolsAnd_numbers": 10,
    }

    expect(isObject(input)).to.be.true
  })

  it("should return false if string", () => {
    const input = "this is a string"

    expect(isObject(input)).to.be.false
  })

  it("should return false if number", () => {
    const input = 5

    expect(isObject(input)).to.be.false
  })

  it("should return false if array", () => {
    const input = ["this is a string"]

    expect(isObject(input)).to.be.false
  })

  it("should return false if function", () => {
    const input = () => {}

    expect(isObject(input)).to.be.false
  })

  it("should return false if null", () => {
    const input = null

    expect(isObject(input)).to.be.false
  })

  it("should return false if undefined", () => {
    const input = undefined

    expect(isObject(input)).to.be.false
  })
})

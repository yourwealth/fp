const chai = require("chai")
const {expect} = chai
const mapObjKeys = require("../src/map-obj-keys")

describe("mapObjKeys", () => {

  it("should be a function", () => {
    expect(mapObjKeys).to.be.instanceOf(Function)
  })

  it("should be a curried function", () => {
    expect(mapObjKeys(()=>{})).to.be.instanceOf(Function)
  })

  it("should return an object with converted keys", () => {
    const input = {
      thisHASaMixOfUpPeraNdLoWeR: 5,
      thisisalllower: 10,
      THISISALLUPPER: 1,
      this5HAs9$aCoMBo: 11,
    }
    const mappingFunction = input =>
      input.toLowerCase()

    const expected = {
      thishasamixofupperandlower: 5,
      thisisalllower: 10,
      thisisallupper: 1,
      this5has9$acombo: 11,
    }
    expect(mapObjKeys(mappingFunction, input)).to.deep.equal(expected)
  })

  it("should throw when array", () => {
    const input = [{
      thisHASaMixOfUpPeraNdLoWeR: 5,
      thisisalllower: 10,
      THISISALLUPPER: 1,
      this5HAs9$aCoMBo: 11,
    }]
    const mappingFunction = input =>
      input.toLowerCase()

    expect(() => mapObjKeys(mappingFunction, input)).to.throw("mapObjKeys error: Provided input is not an object")
  })

  it("should throw when string", () => {
    const input = "test_this"
    const mappingFunction = input =>
      input.toLowerCase()
    expect(() => mapObjKeys(mappingFunction, input)).to.throw("mapObjKeys error: Provided input is not an object")
  })

  it("should throw when number", () => {
    const input = 5
    const mappingFunction = input =>
      input.toLowerCase()
    expect(() => mapObjKeys(mappingFunction, input)).to.throw("mapObjKeys error: Provided input is not an object")
  })

  it("should throw when null", () => {
    const input = null
    const mappingFunction = input =>
      input.toLowerCase()
    expect(() => mapObjKeys(mappingFunction, input)).to.throw("mapObjKeys error: Provided input is not an object")
  })

  it("should throw when undefined", () => {
    const input = undefined
    const mappingFunction = input =>
      input.toLowerCase()
    expect(() => mapObjKeys(mappingFunction, input)).to.throw("mapObjKeys error: Provided input is not an object")
  })
})

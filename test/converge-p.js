const {expect} = require("chai")
const {mergeAll, unapply, objOf} = require("ramda")
const {convergeAny} = require("../src/")
const Promise = require("bluebird")

describe("convergeAny", () =>  {

  const voidFunc = () => {}

  it("is a function",
    () => expect(convergeAny).to.be.a("function"))

  it("returns a Promise",
    () => expect(convergeAny(voidFunc, [voidFunc])({})).to.be.instanceOf(Promise))

  it("converges function arguments", () => {
    return convergeAny(
      (a, b) => a + b,
      [
        (y) => y * 7,
        (x) => x * 5,
      ]
    )(3)
      .then(result => {
        expect(result).to.equal(36)
      })
  })

  it("converges functions which return Promises", () => {
    const userId = 1

    const getUser = userId => Promise.resolve({id: userId, name: "Mark"})
    const getAddress = userId => Promise.resolve({id: userId, address: "20 Street"})
    const combineUserAddress = unapply(mergeAll)

    return convergeAny(
      combineUserAddress,
      [
        getUser,
        getAddress,
      ]
    )(userId)
      .then(result => {
        expect(result).to.eql({
          id: userId,
          name: "Mark",
          address: "20 Street",
        })
      })
  })

  it("converges a mixture of functions which return Promises and values", () => {
    const userId = 1

    // async
    const getUser = userId => Promise.resolve({id: userId, name: "Mark"})
    const getAddress = userId => Promise.resolve({id: userId, address: "20 Street"})

    // sync
    const generateUserSecret = () => objOf("secret", "Likes turnips")
    const combineUserAddress = unapply(mergeAll)

    return convergeAny(
      combineUserAddress,
      [
        getUser,
        getAddress,
        generateUserSecret,
      ]
    )(userId)
      .then(result => {
        expect(result).to.eql({
          id: userId,
          name: "Mark",
          address: "20 Street",
          secret: "Likes turnips",
        })
      })
  })
})

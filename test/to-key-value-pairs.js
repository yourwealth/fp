const {expect} = require("chai")
const {toKeyValuePairs} = require("../src/")

describe("toKeyValuePairs", () =>  {
  it("is a function", () => {
    expect(toKeyValuePairs).to.be.a("function")
  })

  it("converts an object with a single property to a length 1 list", () => {
    expect(toKeyValuePairs({foo: "bar"})).to.eql([{key: "foo", value: "bar"}])
  })

  it("converts an object with two properties to a length 2 list", () => {
    expect(toKeyValuePairs({foo: "bar", baz: "faz"})).to.eql([
      {key: "foo", value: "bar"},
      {key: "baz", value: "faz"},
    ])
  })

  it("works with number keys, but converts to string", () => {
    expect(toKeyValuePairs({2: "bar"})).to.eql([{key: "2", value: "bar"}])
  })
})

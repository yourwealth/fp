const chai = require("chai")
const {expect} = chai
const mapObjKeysToCamelCase = require("../src/map-obj-keys-to-camel-case")

describe("mapObjKeysToCamelCase", () => {

  it("should be a function", () => {
    expect(mapObjKeysToCamelCase).to.be.instanceOf(Function)
  })

  it("should return an object with converted keys", () => {
    const input = {
      thisIsCamelCase: 5,
      "this-is-kebab-case": 2,
      "this_is_snake_case": 6,
      "this5@_has4some$symbolsAnd_numbers": 10,
    }
    const expected = {
      thisIsCamelCase: 5,
      thisIsKebabCase: 2,
      thisIsSnakeCase: 6,
      this5Has4SomeSymbolsAndNumbers: 10,
    }
    expect(mapObjKeysToCamelCase(input)).to.deep.equal(expected)
  })

  it("should throw when array", () => {
    const input = [{
      thisHASaMixOfUpPeraNdLoWeR: 5,
      thisisalllower: 10,
      THISISALLUPPER: 1,
      this5HAs9$aCoMBo: 11,
    }]
    expect(() => mapObjKeysToCamelCase(input)).to.throw("mapObjKeys error: Provided input is not an object")
  })

  it("should throw when string", () => {
    const input = "test_this"
    expect(() => mapObjKeysToCamelCase(input)).to.throw("mapObjKeys error: Provided input is not an object")
  })

  it("should throw when number", () => {
    const input = 5
    expect(() => mapObjKeysToCamelCase(input)).to.throw("mapObjKeys error: Provided input is not an object")
  })

  it("should throw when null", () => {
    const input = null
    expect(() => mapObjKeysToCamelCase(input)).to.throw("mapObjKeys error: Provided input is not an object")
  })

  it("should throw when undefined", () => {
    const input = undefined
    expect(() => mapObjKeysToCamelCase(input)).to.throw("mapObjKeys error: Provided input is not an object")
  })
})

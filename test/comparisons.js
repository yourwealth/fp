const {greaterThan, lessThan, greaterThanOrEqual, lessThanOrEqual} = require("../src/comparisons")
const {expect} = require("chai")

describe("greaterThan", () => {
  it("should be a function", () => {
    expect(greaterThan).to.be.instanceOf(Function)
  })

  it("should be a curried function", () => {
    expect(greaterThan(1)).to.be.instanceOf(Function)
  })

  it("should evaluate greaterThan(a)(b) such that it asks: is b greater than a?", () => {
    expect(greaterThan(1)(2)).to.be.equal(true)
    expect(greaterThan(1)(1)).to.be.equal(false)
  })

  it("should evaluate strings by their natural ordering", () => {
    expect(greaterThan("a")("b")).to.be.equal(true)
    expect(greaterThan("a")("a")).to.be.equal(false)
  })
})

describe("greaterThanOrEqual", () => {
  it("should be a function", () => {
    expect(greaterThanOrEqual).to.be.instanceOf(Function)
  })

  it("should be a curried function", () => {
    expect(greaterThanOrEqual(1)).to.be.instanceOf(Function)
  })

  it("should evaluate greaterThan(a)(b) such that it asks: is b greater than or equal to a?", () => {
    expect(greaterThanOrEqual(1)(2)).to.be.equal(true)
    expect(greaterThanOrEqual(1)(1)).to.be.equal(true)
    expect(greaterThanOrEqual(1)(0)).to.be.equal(false)
  })

  it("should evaluate strings by their natural ordering", () => {
    expect(greaterThanOrEqual("a")("b")).to.be.equal(true)
    expect(greaterThanOrEqual("a")("a")).to.be.equal(true)
    expect(greaterThanOrEqual("b")("a")).to.be.equal(false)
  })
})

describe("lessThan", () => {
  it("should be a function", () => {
    expect(lessThan).to.be.instanceOf(Function)
  })

  it("should be a curried function", () => {
    expect(lessThan(1)).to.be.instanceOf(Function)
  })

  it("should evaluate lessThan(a)(b) such that it asks: is b less than a?", () => {
    expect(lessThan(1)(1)).to.be.equal(false)
    expect(lessThan(1)(0)).to.be.equal(true)
  })

  it("should evaluate strings by their natural ordering", () => {
    expect(lessThan("b")("b")).to.be.equal(false)
    expect(lessThan("b")("a")).to.be.equal(true)
  })
})

describe("lessThanOrEqual", () => {
  it("should be a function", () => {
    expect(lessThanOrEqual).to.be.instanceOf(Function)
  })

  it("should be a curried function", () => {
    expect(lessThanOrEqual(1)).to.be.instanceOf(Function)
  })

  it("should evaluate lessThan(a)(b) such that it asks: is b less than or equal to a?", () => {
    expect(lessThanOrEqual(0)(1)).to.be.equal(false)
    expect(lessThanOrEqual(0)(0)).to.be.equal(true)
    expect(lessThanOrEqual(0)(-1)).to.be.equal(true)
  })

  it("should evaluate strings by their natural ordering", () => {
    expect(lessThanOrEqual("b")("b")).to.be.equal(true)
    expect(lessThanOrEqual("b")("a")).to.be.equal(true)
    expect(lessThanOrEqual("b")("c")).to.be.equal(false)
  })
})

const {inRange, notInRange} = require("../src/in-range")
const {expect} = require("chai")

describe("inRange", () => {
  it("should be a function", () => {
    expect(inRange).to.be.instanceOf(Function)
  })

  it("should return a function, curry_3", () => {
    expect(inRange(1)).to.be.instanceOf(Function)
    expect(inRange(1)(2)).to.be.instanceOf(Function)
    expect(inRange(1, 2)).to.be.instanceOf(Function)
  })

  it("should return true when val is in range, where max is exclusive", () => {
    expect(inRange(1, 2)(1)).to.equal(true)
    expect(inRange(1, 3)(2)).to.equal(true)
    expect(inRange("a", "c")("b")).to.equal(true)
    expect(inRange("a", "c")("a")).to.equal(true)
  })

  it("should return false when val is not in range, where max is exclusive", () => {
    expect(inRange(1, 2)(2)).to.equal(false)
    expect(inRange(1, 3)(0)).to.equal(false)
    expect(inRange("b", "d")("d")).to.equal(false)
    expect(inRange("b", "c")("a")).to.equal(false)
  })
})

describe("notInRange", () => {
  it("should be a function", () => {
    expect(notInRange).to.be.instanceOf(Function)
  })

  it("should return a function, curry_3", () => {
    expect(notInRange(1)).to.be.instanceOf(Function)
    expect(notInRange(1)(2)).to.be.instanceOf(Function)
    expect(notInRange(1, 2)).to.be.instanceOf(Function)
  })

  it("should return true when val is in range, where max is exclusive", () => {
    expect(notInRange(1, 2)(2)).to.equal(true)
    expect(notInRange(1, 3)(0)).to.equal(true)
    expect(notInRange("b", "d")("d")).to.equal(true)
    expect(notInRange("b", "c")("a")).to.equal(true)
  })

  it("should return false when val is not in range, where max is exclusive", () => {
    expect(notInRange(1, 2)(1)).to.equal(false)
    expect(notInRange(1, 3)(2)).to.equal(false)
    expect(notInRange("a", "c")("b")).to.equal(false)
    expect(notInRange("a", "c")("a")).to.equal(false)
  })
})

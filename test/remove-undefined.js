const {expect} = require("chai")
const {removeUndefined} = require("../src/")

describe("removeUndefined", () =>  {
  it("should remove all undefined values from an object", () => {
    expect(removeUndefined({
      foo: "1",
      bar: undefined,
      bax: 22,
    })).to.eql({
      foo: "1",
      bax: 22,
    })
  })
})

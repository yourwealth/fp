const {expect} = require("chai")
const {updateWhere} = require("../src")

describe("updateWhere", function() {
  it("should update items in an array based on a set of find params", function() {
    const data = [{uid: 1, deleted: false}, {uid: 2, deleted: false}, {uid: 3, deleted: false}]
    const res = updateWhere({uid: 2}, {deleted: true}, data)

    expect(res).to.eql([{uid: 1, deleted: false}, {uid: 2, deleted: true}, {uid: 3, deleted: false}])
  })
})

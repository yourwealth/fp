const {expect} = require("chai")
const {throwWhenAny} = require("../src/")

const voidPredicate = () => {}
const truePredicate = () => true
const falsePredicate = () => false

describe("throwWhenAny", () => {

  it("is a function",
    () => expect(throwWhenAny).to.be.a("function"))

  it("returns a function",
    () => expect(throwWhenAny(voidPredicate, "")).to.be.a("function"))

  it("throws an error when passed a predicate which evaluates to true",
    () => expect(() => throwWhenAny(truePredicate, new Error())([null])).to.throw(Error)
  )

  it("returns the supplied array when passed a predicate which evaluates to false",
    () => expect(throwWhenAny(falsePredicate, "")([1, 2, 3])).to.eql([1, 2, 3])
  )

})

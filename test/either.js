const {expect} = require("chai")
const {either} = require("./../src/")

describe("either", () => {
  it("is a function", () => {
    expect(either).to.be.a("function")
  })

  it("returns a function", () => {
    const receiver = either(() => {})
    expect(receiver).to.be.a("function")
  })

  it("the receiver will return the evaluation if it is not null|undefined, or the original argument", () => {
    const receiver = either((x) => {
      if (x === 1) {
        return "rumpelstiltskin"
      }
      return undefined
    })

    expect(receiver(1)).to.equal("rumpelstiltskin")
    expect(receiver(2)).to.equal(2)
  })
})

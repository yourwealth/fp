const {expect} = require("chai")
const {isNilOrEmpty} = require("./../src/")

describe("isNilOrEmpty", () => {
  it("is a function", () => {
    expect(isNilOrEmpty).to.be.a("function")
  })

  it("returns a boolean", () => {
    const actual = typeof(isNilOrEmpty({}))
    const expected = typeof(true)
    expect(actual).to.equal(expected)
  })

  it("returns false if argument is not nil or empty", () => {
    const actual = isNilOrEmpty({user: 123})
    const expected = false
    expect(actual).to.equal(expected)
  })

  it("returns true if argument is nil", () => {
    const actual = isNilOrEmpty(null)
    const expected = true
    expect(actual).to.equal(expected)
  })

  it("returns true if argument is nil", () => {
    const actual = isNilOrEmpty(undefined)
    const expected = true
    expect(actual).to.equal(expected)
  })

  it("returns true if argument is empty", () => {
    const actual = isNilOrEmpty({})
    const expected = true
    expect(actual).to.equal(expected)
  })

  it("returns true if argument is empty string", () => {
    const actual = isNilOrEmpty("")
    const expected = true
    expect(actual).to.equal(expected)
  })

  it("returns true if argument is empty array", () => {
    const actual = isNilOrEmpty([])
    const expected = true
    expect(actual).to.equal(expected)
  })
})
